import {Component, Input} from '@angular/core';
import {UserData} from '../../interface/user-data.interface';

@Component({
  selector: 'app-user-box',
  templateUrl: './user-box.component.html',
  styles: []
})
export class UserBoxComponent {

  @Input()
  userData: UserData;
}
