import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {Subscription} from 'rxjs';
import {DragDropService} from '../../service/drag-drop.service';

@Component({
  selector: 'app-drop-area',
  templateUrl: './drop-area.component.html',
  animations: [
    trigger('activeArea', [
      state('active', style({
        'border-width': '3px',
        height: '40px',
        opacity: 1
      })),
      state('inactive', style({
        'border-width': '2px',
        height: '10px',
        opacity: 0.5
      })),
      transition('inactive => active', [
        animate('0.25s')
      ]),
      transition('active => inactive', [
        animate('0.5s')
      ]),
    ]),
  ],
})

export class DropAreaComponent implements OnInit, OnDestroy {

  @Input()
  dropElementPosition: number;

  @Output()
  dropElement = new EventEmitter<number>();

  activeIndex: number;

  subscription: Subscription;

  constructor(private dragDropService: DragDropService) {
  }

  ngOnInit(): void {
    this.subscription = this.dragDropService.getLastActiveAreaObservable()
      .subscribe(activeIndex => this.activeIndex = activeIndex);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  dragEnter() {
    this.dragDropService.setLastActiveArea(this.dropElementPosition);
  }
}
