import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable()
export class DragDropService {

  draggedElementIndex: number;

  private lastActiveArea = new BehaviorSubject<number>(null);

  reset() {
    this.draggedElementIndex = null;
    this.lastActiveArea.next(null);
  }

  getLastActiveAreaObservable() {
    return this.lastActiveArea.asObservable();
  }

  setLastActiveArea(index: number) {
    this.lastActiveArea.next(index);
  }
}
