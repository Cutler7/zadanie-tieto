import {Injectable} from '@angular/core';
import {UserData} from '../interface/user-data.interface';

@Injectable()
export class MockHttpService {

  private readonly userList: UserData[] = [
    {id: 1, name: 'Jan Kowalski', email: 'janek@test.com', points: 100},
    {id: 2, name: 'Paweł Nowak', email: 'pawel@test.com', points: 650},
    {id: 3, name: 'Marzena Kowalczyk', email: 'marzena@test.com', points: 200},
    {id: 4, name: 'Waldemar Kiepski', email: 'waldek@test.com', points: 250},
    {id: 5, name: 'Aneta Malinowska', email: 'aneta@test.com', points: 380},
    {id: 6, name: 'Piotr Wiśniewski', email: 'piotrek@test.com', points: 800},
    {id: 7, name: 'Krzysztof Zając', email: 'krzysiek@test.com', points: 910},
    {id: 8, name: 'Magdalena Wróbel', email: 'magda@test.com', points: 1050},
    {id: 9, name: 'Anna Jabłońska', email: 'ania@test.com', points: 420},
    {id: 10, name: 'Mateusz Jankowski', email: 'mateusz@test.com', points: 780}
  ];

  public getUserList(): Promise<UserData[]> {
    return Promise.resolve(this.userList);
  }
}
