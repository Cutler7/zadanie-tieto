import {Component, OnInit} from '@angular/core';
import {MockHttpService} from './service/mock-http.service';
import {UserData} from './interface/user-data.interface';
import {DragDropService} from './service/drag-drop.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {

  userList: UserData[] = [];

  constructor(private mockHttpService: MockHttpService,
              private dragDropService: DragDropService) {
  }

  ngOnInit(): void {
    this.mockHttpService.getUserList()
      .then(res => this.userList = res);
  }

  dragStart(index: number) {
    this.dragDropService.draggedElementIndex = index;
  }

  dragEnd() {
    this.dragDropService.reset();
  }

  reorderList(targetIndex: number) {
    const sourceIndex = this.dragDropService.draggedElementIndex;
    const movedElement = this.userList[sourceIndex];
    this.userList.splice(sourceIndex, 1);
    this.userList.splice(sourceIndex < targetIndex ? targetIndex - 1 : targetIndex , 0, movedElement);
    this.dragDropService.reset();
  }
}
