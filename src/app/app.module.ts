import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {MockHttpService} from './service/mock-http.service';
import {UserBoxComponent} from './component/user-box/user-box.component';
import {DropAreaComponent} from './component/drop-area/drop-area.component';
import {DragDropModule} from 'primeng/primeng';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DragDropService} from './service/drag-drop.service';

@NgModule({
  declarations: [
    AppComponent,
    UserBoxComponent,
    DropAreaComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,

    DragDropModule
  ],
  providers: [
    MockHttpService,
    DragDropService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
