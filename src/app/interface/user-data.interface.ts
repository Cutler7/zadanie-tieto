export interface UserData {

  id: number;

  name: string;

  email: string;

  points: number;

}
